//----------------------------------------------------------------------------
// IPG Laser YLR-200-LP-WC HELPER CLASS
//----------------------------------------------------------------------------
//
// Copyright (C) 2014 The SOLEIL Community
//
// Contact:
//      Sy-moran Trem
//      Synchrotron SOLEIL
//------------------------------------------------------------------------------

/*!
 *  \file       DG645Task.cpp
 *  \brief      DG 645  class
 *  \author     sy-moran trem
 *  \date       8th july 2014
 *  \version    1.0
 */

#include "DG645Task.h"
#include <math.h>
#include <TangoExceptionsHelper.h>
#include <yat4tango/YatLogAdapter.h>
#include <yat/utils/XString.h>

namespace DG645_ns
{

const StatusMap DG645Task::status_manager;


//+----------------------------------------------------------------------------
//
// method :                 DG645Task::DG645Task
//
// description :            DG645Task Constructor
//
//-----------------------------------------------------------------------------
DG645Task::DG645Task(Tango::DeviceImpl* host_dev, std::string socket_proxy):
    yat4tango::DeviceTask(host_dev),
    m_host_dev(host_dev),
    m_socket_proxy(0),
    m_is_socket_alive(false),
    m_is_task_init(false),
    m_prescale(5),
    m_delay(10),
    m_width(4),
    m_burst_count(0)
{
    // DEBUG_STREAM << "In DG645Task::DG645Task" << endl;

    try
    {
        yat4tango::DeviceTask::enable_timeout_msg(false);   //- No need of TASK_TIMEOUT message type
        yat4tango::DeviceTask::enable_periodic_msg(true);   //- Need of TASK_PERIODIC message type
        yat4tango::DeviceTask::set_periodic_msg_period(MESSAGE_PERIOD);

        //- Creating the m_socket_proxy
        m_socket_proxy = new Tango::DeviceProxyHelper(socket_proxy, m_host_dev);
        m_socket_proxy->get_device_proxy()->ping();

        m_is_socket_alive = true;
        m_is_task_init = true;
    }
    catch ( const Tango::DevFailed& df )
    {
        m_is_socket_alive = false;
    }
    
    state_status_update();
}

//+----------------------------------------------------------------------------
//
// method :                 DG645Task::DG645Task
//
// description :            DG645Task Destructor
//
//-----------------------------------------------------------------------------
DG645Task::~DG645Task()
{
    // DEBUG_STREAM << "In DG645Task::~DG645Task" << endl;

    try
    {
        yat4tango::DeviceTask::enable_periodic_msg(false);
        yat4tango::DeviceTask::clear_pending_messages();    //- Clears all pending messages in the message queue.

        if( m_socket_proxy )
        {
            delete m_socket_proxy;
            m_socket_proxy = 0;
        }
    }
    catch ( const Tango::DevFailed& df )
    {
        //- do nothing
        ERROR_STREAM << "DG645Task::~DG645Task -> df:\n" << df << std::endl;
    }
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::state_status_update
//
//-----------------------------------------------------------------------------
void DG645Task::state_status_update()
{
    // DEBUG_STREAM << "state_status_update(): Entering";
    // DEBUG_STREAM << "In DG645Task::state_status_update" <<endl;
    int logging_level = Tango::OFF;
    try
    {
        //- FAULT & INIT state
        //- If the Socket failed to initialize
        if( !m_is_task_init )
        {
            m_state = Tango::FAULT;
            m_status.str("Initialization failed : DG645Task");
        }
        else if( !m_is_socket_alive )
        {
/*          INFO_STREAM << "state_status_update(): else if( !m_is_socket_alive )";
            Tango::DeviceProxy auto_proxy( m_host_dev->get_name() );
            logging_level = auto_proxy.get_logging_level();
            
            // We are already warned that the device is not responding, test if the proxy is now alive
            m_socket_proxy->get_device_proxy()->set_logging_level(Tango::OFF);*/
            m_is_socket_alive = true;
            INFO_STREAM << "state_status_update(): m_is_socket_alive = true";
        }
        else //- Usual state & status
        {
            m_status.str("");

            //- Gettings the last 19th key values errors
            std::string response_number = dg645_command_inout("LERR?");
            if( response_number != "0" )
            {
                // DEBUG_STREAM << "Key value(s) : " << response_number << std::endl;

                std::stringstream unknown_error;
                unknown_error << "key value <" << response_number << "> returned by LERR? is unknown" << endl;
            
                //- Getting the status related to the key values
                std::string response = status_manager.get_status(response_number, unknown_error.str());

                m_state = Tango::FAULT;
                dg645_command("XTRM13");
                ERROR_STREAM << "DG645Task::state_status_update command returned by LERR? : <"<< response << ">" << endl;
                m_status.str(response);
            }
            else
            {
                m_state = Tango::STANDBY;
                m_status.str("Normal operation");
            }
        }
            
    }//- End of try
    catch ( const Tango::DevFailed& df )
    {
        // DEBUG_STREAM << "state_status_update(): catch ( const Tango::DevFailed& df )";
        if( m_is_socket_alive || m_is_task_init )
        {
            m_state = Tango::OFF;
            m_status.str("The ClientSocketDevice is not responding");
            INFO_STREAM << "state_status_update(): The ClientSocketDevice is not responding";
        }
        //m_is_socket_alive = false;
        INFO_STREAM << "m_is_socket_alive = false";
    }
    if( logging_level != Tango::OFF )
    {
        Tango::DeviceProxy auto_proxy( m_host_dev->get_name() );
        auto_proxy.set_logging_level( logging_level );
    }
    INFO_STREAM << "state_status_update(): Quiting";
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::dg645_command_inout
//
//+----------------------------------------------------------------------------
std::string DG645Task::dg645_command_inout(std::string const& cmd_to_send)
{
    // DEBUG_STREAM << "In DG645Task::dg645_command_inout " << cmd_to_send << endl;

    std::string response("");

    try
    {
        if( m_is_socket_alive )
        {
            //- Getting the generated response & sending the command, and erase Carriage Return
            m_socket_proxy->command_inout("WriteRead", cmd_to_send, response);
            m_is_socket_alive = true;
            response.resize(response.size() - 1);
        }
    }
    catch ( const Tango::DevFailed& df )
    {
        if( m_is_socket_alive )
        {
            m_state = Tango::OFF;
            m_status.str("The ClientSocketDevice is not responding");
            // DEBUG_STREAM << "state_status_update(): The ClientSocketDevice is not responding";
        }
        // DEBUG_STREAM << "dg645_command_inout(): is_socket_alive = false";
        //m_is_socket_alive = false;
        // _TANGO_TO_YAT_EXCEPTION( df, ex );
        // YAT_LOG_EXCEPTION( ex );
    }
    return response;
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::dg645_command
//
//+----------------------------------------------------------------------------
void DG645Task::dg645_command(std::string const& cmd_to_send)
{
    // DEBUG_STREAM << "In DG645Task::dg645_command" <<endl;

    try
    {
        if( m_is_socket_alive )
        {
            //- Getting the generated response & sending the command
            m_socket_proxy->command_in("Write", cmd_to_send);
            m_is_socket_alive = true;
        }
    }
    catch ( const Tango::DevFailed& df )
    {
        if( m_is_socket_alive )
        {
            m_state = Tango::OFF;
            m_status.str("The ClientSocketDevice is not responding");
            // DEBUG_STREAM << "state_status_update(): The ClientSocketDevice is not responding";
        }
        // DEBUG_STREAM << "dg645_command_inout(): is_socket_alive = false";
        //m_is_socket_alive = false;
        // DEBUG_STREAM << "dg645_command(): is_socket_alive = false";
        // m_is_socket_alive = false;
        // _TANGO_TO_YAT_EXCEPTION( df, ex );
        // YAT_LOG_EXCEPTION( ex );
    }
}


//+----------------------------------------------------------------------------
//
// Internal method :       DG645Task::has_bit_set
//
//-----------------------------------------------------------------------------
inline bool DG645Task::has_bit_set(unsigned char byte, size_t bit)
{
    return ( (byte>>bit) & 1 );
}

//+----------------------------------------------------------------------------
//
// Internal method :       DG645Task::set_bit
//
//-----------------------------------------------------------------------------
inline void DG645Task::set_bit(unsigned char& byte, size_t bit)
{
    byte |= (unsigned char)1 << bit; 
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::prescale_update
//
// description :            Turning Tango::DevState into string
//
//-----------------------------------------------------------------------------
inline void DG645Task::prescale_update()
{
    static const std::string prescale_index = "01234";

    //- m_prescale[0]   Trigger input
    //- m_prescale[1]   Output AB
    //- m_prescale[2]   Output CD
    //- m_prescale[3]   Output EF
    //- m_prescale[4]   Output GH

    std::string str = dg645_command_inout("ADVT?");
    // DEBUG_STREAM << "ADVT : " << str << std::endl;
    if( str == "1" )
    {
        //- Resetting m_prescale
        std::fill(m_prescale.begin(), m_prescale.end(), -1);

        //- Updating the m_prescale data
        for(size_t i=0; i < prescale_index.size(); i++)
        {
            std::stringstream sstr;
            sstr << "PRES?" << prescale_index[i];

            std::stringstream( dg645_command_inout(sstr.str()) ) >> m_prescale[i];
            // DEBUG("DG645Task::prescale_update :<" + sstr.str() +">");
        }
    }
    else
    {
       std::fill(m_prescale.begin(), m_prescale.end(), 1);
    }
}

//+----------------------------------------------------------------------------
//
// Internal method :       DG645Task::get_sum
//
//-----------------------------------------------------------------------------
double DG645Task::get_sum(size_t channel, const delay_storage& data, unsigned char &visited)
{
    if (has_bit_set(visited, channel) || channel >= m_delay.size())
    {
        throw 0;
    }
    set_bit(visited, channel);

    double current_delay = m_delay[channel];
    if (current_delay != -1)
    {
        return current_delay;
    }

    double retval =  data[channel].second;
    size_t next_channel = data[channel].first;
    if (channel != 0)
    {
        retval += get_sum(next_channel, data, visited);
    }

    return retval;
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::channel_update
//
//-----------------------------------------------------------------------------
inline void DG645Task::channel_update(size_t channel, const delay_storage& data)
{
    unsigned char visited=0;
    m_delay[channel] = get_sum(channel, data, visited);
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::delay_update
//
// description :            Turning Tango::DevState into string
//
//-----------------------------------------------------------------------------
inline void DG645Task::delay_update()
{
    static const std::string delay_index = "0123456789";
    static const std::string separator(",+");

    //- m_delay[0]   T0 Channel
    //- m_delay[1]   T1 Channel
    //- m_delay[2]   A Channel
    //- m_delay[3]   B Channel
    //- m_delay[4]   C Channel
    //- m_delay[5]   D Channel
    //- m_delay[6]   E Channel
    //- m_delay[7]   F Channel
    //- m_delay[8]   G Channel
    //- m_delay[9]   H Channel  "x, delay"

    delay_storage data(10);
    for(size_t i=0 ; i < data.size() ; i++)
    {
        std::stringstream sstr;
        sstr << "DLAY?" << delay_index[i];

        std::string response = dg645_command_inout(sstr.str());
     
        //- Getting the channel number
        size_t channel;
        std::string tmp_str(response);
        std::size_t pos = response.find(separator);
        std::istringstream respVal;
        respVal.str(response);
        tmp_str.resize(0, pos);
        respVal >> channel;
        data[i].first = channel;

        //- Getting the delay value
        double delay = 0.;
        response.erase(0, pos+separator.size());    //- Erasing the channel indication
        response.erase(response.size());          //- Erasing the CR
        //- clear previous response
        respVal.str();
        respVal.str(response);
        respVal >> delay;
        delay = delay * 1.0e+09;
        data[i].second = delay;
    }

    // reset
    std::fill(m_delay.begin(), m_delay.end(), -1);
    for(size_t i=0 ; i < m_delay.size(); i++)
    {
      try
      {
        channel_update(i, data);
      }
      catch(...)
      {
        break;
      }
    }
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::width_update
//
//-----------------------------------------------------------------------------
inline void DG645Task::width_update()
{
    //- m_width[0]   Output AB
    //- m_width[1]   Output CD
    //- m_width[2]   Output EF
    //- m_width[3]   Output GH

    //- Updating the m_width data    
    m_width[0] = fabs(m_delay[3] - m_delay[2]);
    m_width[1] = fabs(m_delay[5] - m_delay[4]);
    m_width[2] = fabs(m_delay[7] - m_delay[6]);
    m_width[3] = fabs(m_delay[9] - m_delay[8]);
}

//+----------------------------------------------------------------------------
//
// Internal method :        DG645Task::burst_count_update
//
//-----------------------------------------------------------------------------
inline void DG645Task::burst_count_update()
{
  std::string response = dg645_command_inout("BURC?");
  //- TODO: clean response
//  std::string data_str = response.erase(response.find("\n"));
//std::cout << "\t data_str: " << data_str << std::endl;
  //- convert response
  m_burst_count = yat::XString<Tango::DevUShort>::to_num (response);
// std::cout << "\t m_burst_count: " << m_burst_count << std::endl;
}
//+----------------------------------------------------------------------------
//
// Protected method :       DG645Task::process_message
//
// description :            Implementation of a pure virtual method from yat4tango::DeviceTask
//
//-----------------------------------------------------------------------------
void DG645Task::process_message(yat::Message& msg) throw (Tango::DevFailed)
{
    try
    {
        m_status.str("");
		//   //- AutoMutex, auto lock and unlock mechanism
		yat::AutoMutex<> guard(m_dg645_lock);

        switch ( msg.type() )
        {
        //-----------------------------------------------------------------------------------------------
            case yat::TASK_INIT:
                // DEBUG_STREAM << "DG645Task -> yat::TASK_INIT" << endl;
                
                m_get = msg.get_data<CurrentsConfig>();
                m_status.str("DG645 Task is waiting for request...");

                //- Check if the socketdevice is initialised
                if (m_is_task_init)
                {
                    dg645_command("*RCL0");
                    dg645_command("ADVT1");
                    dg645_command("XTRM13");    //- Setting the terminator response (13: Carriage_return)
                }
                break;

        //-----------------------------------------------------------------------------------------------
            //- GETTING CASE, Called periodically
            case yat::TASK_PERIODIC:
              // INFO_STREAM << "DG645Task -> TASK_PERIODIC" << endl;                
                
                state_status_update();
                
                if ( m_is_task_init && m_is_socket_alive )
                {
                    prescale_update();
                    delay_update();
                    width_update();
                    burst_count_update();
                    dg645_command("LCAL");
                }
                m_get.is_socket_alive = m_is_socket_alive;
                m_get.status = m_status.str();
                m_get.state = m_state;

                //- DEFAULT DEBUG PANNEL
                // DEBUG_STREAM << "\n"
                             // << "\nIS_SOCKET_ALIVE    : " << m_get.is_socket_alive 
                             // << "\nSTATUS            : " << m_get.status
                             // << "\nSTATE             : " << Tango::DevStateName[m_get.state];

				m_get.freq_div       = m_prescale[0];
				m_get.freq_div_ab    = m_prescale[1];
				m_get.freq_div_cd    = m_prescale[2];
				m_get.freq_div_ef    = m_prescale[3];
				m_get.freq_div_gh    = m_prescale[4];

				m_get.delay_a        = m_delay[2];
				m_get.delay_b        = m_delay[3];
				m_get.delay_c        = m_delay[4];
				m_get.delay_d        = m_delay[5];
				m_get.delay_e        = m_delay[6];
				m_get.delay_f        = m_delay[7];
				m_get.delay_g        = m_delay[8];
				m_get.delay_h        = m_delay[9];

				m_get.width_ab       = m_width[0];
				m_get.width_cd       = m_width[1];
				m_get.width_ef       = m_width[2];
				m_get.width_gh       = m_width[3];
        
        m_get.burst_count    = m_burst_count;

                //- COMPLETE DEBUG PANNEL
                // DEBUG_STREAM << "\n"
                //              << "\nFREQ DIV         : " << m_get.freq_div
                //              << "\nFREQ DIV AB      : " << m_get.freq_div_ab
                //              << "\nDELAY A          : " << m_get.delay_a
                //              << "\nDELAY B          : " << m_get.delay_b
                //              << "\nWIDTH AB         : " << m_get.width_ab
                //              << "\nFREQ DIV CD      : " << m_get.freq_div_cd
                //              << "\nDELAY C          : " << m_get.delay_c
                //              << "\nDELAY D          : " << m_get.delay_d
                //              << "\nWIDTH CD         : " << m_get.width_cd
                //              << "\nFREQ DIV EF      : " << m_get.freq_div_ef
                //              << "\nDELAY E          : " << m_get.delay_e
                //              << "\nDELAY F          : " << m_get.delay_f
                //              << "\nWIDTH EF         : " << m_get.width_ef
                //              << "\nFREQ DIV GH      : " << m_get.freq_div_gh
                //              << "\nDELAY G          : " << m_get.delay_g
                //              << "\nDELAY H          : " << m_get.delay_h
                //              << "\nWIDTH GH         : " << m_get.width_gh
                //              << endl;
                break;

        //-----------------------------------------------------------------------------------------------
            //- COMMAND CASE & WRITE ATTRIBUTE CASE
            case COMMAND_MSG:
                dg645_command( msg.get_data<std::string>() );
                break;
                
        //-----------------------------------------------------------------------------------------------
            //- EXIT CASE
            case yat::TASK_EXIT:
                // DEBUG_STREAM << "DG645Task -> yat::TASK_EXIT" << endl;
                break;
        }//- End of switch
    }//- End of try
    catch( const Tango::DevFailed& df )
    {
        _TANGO_TO_YAT_EXCEPTION( df, ex );
        YAT_LOG_EXCEPTION( ex );
    }
    catch( const yat::Exception& e)
    {
        YAT_LOG_EXCEPTION( e );
    }
    catch( const std::exception& e )
    {
        YAT_LOG_ERROR( e.what() );
        //throw;
    }
    catch (...)
    {
        ERROR_STREAM << "DG645Task::process_message : Initialization Failed : caught [UNKNOWN]" << endl;
    }
}

//+----------------------------------------------------------------------------
//
// method :                 DG645Task::get_task_data
//
// description :            Get the currents data in the CurrentsConfig structure
//
//-----------------------------------------------------------------------------
CurrentsConfig DG645Task::get_task_data()
{
    // DEBUG_STREAM << "In DG645Task::get_task_data" <<endl;
    {
        //- AutoMutex, auto lock and unlock mechanism
        yat::AutoMutex<> guard(m_dg645_lock);
        return m_get;
    }
}

//+----------------------------------------------------------------------------
//
// method :                 exit_task
//
// description :            Get the currents data in the CurrentsConfig structure
//
//-----------------------------------------------------------------------------
void exit_task(yat4tango::DeviceTask* t)
{
    try
    {
        //- Automatically post the TASK_EXIT msg! U may never delete manually a DeviceTask !!!
        // cout << "TaskExiter..." << endl;
        if(t)
        {
            t->exit();
        }
        else
        {
            // cout << "Null pointer" << endl;
            throw;
        }
    }
    catch(...)
    {
        // cout << "TaskExiter...Exception" <<endl;
    }
}




}   //- namespace DG645_ns
