//----------------------------------------------------------------------------
// IPG Laser YLR-200-LP-WC HELPER CLASS
//----------------------------------------------------------------------------
//
// Copyright (C) 2014 The SOLEIL Community
//
// Contact:
//      Sy-moran Trem
//      Synchrotron SOLEIL
//------------------------------------------------------------------------------

/*!
 *  \file       DG645Task.cpp
 *  \brief      Laser YLR class
 *  \author     sy-moran trem
 *  \date       8th august 2014
 *  \version    1.0
 */

#ifndef DG645TASK_H
#define DG645TASK_H

#include "DG645TaskConstants.h"
#include <DeviceProxyHelper.h>


namespace DG645_ns
{
class DG645Task : public yat4tango::DeviceTask
{
public:
    /*!
    *   \fn DG645Task()
    *   
    *   \brief DG645Task class constructor
    *   
    *   \param socket_proxy
    */
    DG645Task(Tango::DeviceImpl* host_dev, std::string socket_proxy);
    /*!
    *   \fn ~DG645Task()
    *   
    *   \brief DG645Task class Destructor
    */
    ~DG645Task();

    /*!
    *   \fn get_task_data()
    *   
    *   \brief Get the currents config if the CurrentsConfig structure
    *
    *   \returns CurrentsConfig structure
    */
    CurrentsConfig  get_task_data();
    

protected://- [yat4tango::DeviceTask implementation]
    virtual void    process_message( yat::Message& msg ) throw (Tango::DevFailed);

private:
//- Private constants
    typedef std::vector< std::pair<size_t, double> > delay_storage;
    static const StatusMap                         status_manager;

/*!
*   \fn T my_command()
*   
*   \brief Send a DG645 command and returning the response with the wanted type
*
*   \param cmd_to_send, argin
*
*   \returns Useful response in the wanted type
*/

//- Private methods
/*!
*   \fn dg645_command_inout
*   
*   \brief Internal method used to send a DG645 command, to check and to return the result
*
*   \returns response in string
*/
std::string dg645_command_inout(std::string const& cmd_to_send);

/*!
*   \fn dg645_command
*   
*   \brief Internal method used to send a DG645 command
*/
void        dg645_command(std::string const& cmd_to_send);

/*!
*   \fn state_status_update()
*   
*   \brief Update the DG645 state and status
*/
void        state_status_update();

/*!
*   \fn get_sum()
*   
*   \brief Get the delay related to T0
*
*   \returns double delay
*/
double      get_sum(size_t channel, const delay_storage& data, unsigned char &visited);

/*!
*   \fn has_bit_set()
*/
inline bool has_bit_set(unsigned char byte, size_t bit);

/*!
*   \fn set_bit()
*/
inline void set_bit(unsigned char& byte, size_t bit);

/*!
*   \fn channel_update()
*/
inline void channel_update(size_t channel, const delay_storage& data);

/*!
*   \fn prescale_update()
*/
inline void prescale_update();

/*!
*   \fn delay_update()
*/
inline void delay_update();

/*!
*   \fn width_update()
*/
inline void width_update();

/*!
*   \fn burst_count_update()
*/
inline void burst_count_update();


//- Private members
    Tango::DeviceImpl*          m_host_dev;
    Tango::DeviceProxyHelper*   m_socket_proxy;
    CurrentsConfig              m_get;
    
    bool                        m_is_socket_alive;
    bool                        m_is_task_init;
    Tango::DevState             m_state;
    std::stringstream           m_status;
    yat::Mutex                  m_dg645_lock;
    std::vector<short>          m_prescale;
    std::vector<double>         m_delay;
    std::vector<double>         m_width;
    
    Tango::DevUShort            m_burst_count;
}; //- End of DG645Task class

//-------------------------------------------------------------

void exit_task(yat4tango::DeviceTask* t);


} //namespace DG645Task_ns
#endif
