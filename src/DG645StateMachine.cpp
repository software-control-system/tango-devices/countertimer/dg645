/*----- PROTECTED REGION ID(DG645StateMachine.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        DG645StateMachine.cpp
//
// description : State machine file for the DG645 class
//
// project :     DG645
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include "DG645.h"

/*----- PROTECTED REGION END -----*/	//	DG645::DG645StateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  INIT     |  Device is in INIT.
//  FAULT    |  Device is in FAULT.
//  STANDBY  |  Device is in RUN.
//  OFF      |  Device Socket is OFF


namespace DG645_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : DG645::is_freqDiv_allowed()
 *	Description : Execution allowed for freqDiv attribute
 */
//--------------------------------------------------------
bool DG645::is_freqDiv_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for freqDiv attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::freqDivStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::freqDivStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::freqDivStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::freqDivStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_freqDivAB_allowed()
 *	Description : Execution allowed for freqDivAB attribute
 */
//--------------------------------------------------------
bool DG645::is_freqDivAB_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for freqDivAB attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::freqDivABStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::freqDivABStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::freqDivABStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::freqDivABStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayA_allowed()
 *	Description : Execution allowed for delayA attribute
 */
//--------------------------------------------------------
bool DG645::is_delayA_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayA attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayAStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayAStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayAStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayAStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayB_allowed()
 *	Description : Execution allowed for delayB attribute
 */
//--------------------------------------------------------
bool DG645::is_delayB_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayB attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayBStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayBStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayBStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayBStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_widthAB_allowed()
 *	Description : Execution allowed for widthAB attribute
 */
//--------------------------------------------------------
bool DG645::is_widthAB_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for widthAB attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::widthABStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::widthABStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::widthABStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::widthABStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_freqDivCD_allowed()
 *	Description : Execution allowed for freqDivCD attribute
 */
//--------------------------------------------------------
bool DG645::is_freqDivCD_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for freqDivCD attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::freqDivCDStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::freqDivCDStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::freqDivCDStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::freqDivCDStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayC_allowed()
 *	Description : Execution allowed for delayC attribute
 */
//--------------------------------------------------------
bool DG645::is_delayC_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayC attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayCStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayCStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayCStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayCStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayD_allowed()
 *	Description : Execution allowed for delayD attribute
 */
//--------------------------------------------------------
bool DG645::is_delayD_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayD attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayDStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayDStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayDStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayDStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_widthCD_allowed()
 *	Description : Execution allowed for widthCD attribute
 */
//--------------------------------------------------------
bool DG645::is_widthCD_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for widthCD attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::widthCDStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::widthCDStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::widthCDStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::widthCDStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_freqDivEF_allowed()
 *	Description : Execution allowed for freqDivEF attribute
 */
//--------------------------------------------------------
bool DG645::is_freqDivEF_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for freqDivEF attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::freqDivEFStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::freqDivEFStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::freqDivEFStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::freqDivEFStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayE_allowed()
 *	Description : Execution allowed for delayE attribute
 */
//--------------------------------------------------------
bool DG645::is_delayE_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayE attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayEStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayEStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayEStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayEStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayF_allowed()
 *	Description : Execution allowed for delayF attribute
 */
//--------------------------------------------------------
bool DG645::is_delayF_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayF attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayFStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayFStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayFStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayFStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_widthEF_allowed()
 *	Description : Execution allowed for widthEF attribute
 */
//--------------------------------------------------------
bool DG645::is_widthEF_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for widthEF attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::widthEFStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::widthEFStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::widthEFStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::widthEFStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_freqDivGH_allowed()
 *	Description : Execution allowed for freqDivGH attribute
 */
//--------------------------------------------------------
bool DG645::is_freqDivGH_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for freqDivGH attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::freqDivGHStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::freqDivGHStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::freqDivGHStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::freqDivGHStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayG_allowed()
 *	Description : Execution allowed for delayG attribute
 */
//--------------------------------------------------------
bool DG645::is_delayG_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayG attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayGStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayGStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayGStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayGStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_delayH_allowed()
 *	Description : Execution allowed for delayH attribute
 */
//--------------------------------------------------------
bool DG645::is_delayH_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayH attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::delayHStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::delayHStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::delayHStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::delayHStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_widthGH_allowed()
 *	Description : Execution allowed for widthGH attribute
 */
//--------------------------------------------------------
bool DG645::is_widthGH_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for widthGH attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::widthGHStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::widthGHStateAllowed_WRITE

	//	Check access type.
	if ( type==Tango::READ_REQ )
	{
		//	Compare device state with not allowed states for READ 
		if (get_state()==Tango::INIT ||
			get_state()==Tango::OFF)
		{
		/*----- PROTECTED REGION ID(DG645::widthGHStateAllowed_READ) ENABLED START -----*/
		
		/*----- PROTECTED REGION END -----*/	//	DG645::widthGHStateAllowed_READ
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_burstCount_allowed()
 *	Description : Execution allowed for burstCount attribute
 */
//--------------------------------------------------------
bool DG645::is_burstCount_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for burstCount attribute in Write access.
	/*----- PROTECTED REGION ID(DG645::burstCountStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::burstCountStateAllowed_WRITE

	//	Not any excluded states for burstCount attribute in read access.
	/*----- PROTECTED REGION ID(DG645::burstCountStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::burstCountStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : DG645::is_RecallConfig_allowed()
 *	Description : Execution allowed for RecallConfig attribute
 */
//--------------------------------------------------------
bool DG645::is_RecallConfig_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::OFF)
	{
	/*----- PROTECTED REGION ID(DG645::RecallConfigStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::RecallConfigStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_ResetErrors_allowed()
 *	Description : Execution allowed for ResetErrors attribute
 */
//--------------------------------------------------------
bool DG645::is_ResetErrors_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::INIT ||
		get_state()==Tango::OFF)
	{
	/*----- PROTECTED REGION ID(DG645::ResetErrorsStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::ResetErrorsStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_Trigger_allowed()
 *	Description : Execution allowed for Trigger attribute
 */
//--------------------------------------------------------
bool DG645::is_Trigger_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Trigger command.
	/*----- PROTECTED REGION ID(DG645::TriggerStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::TriggerStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : DG645::is_Burp_allowed()
 *	Description : Execution allowed for Burp attribute
 */
//--------------------------------------------------------
bool DG645::is_Burp_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Burp command.
	/*----- PROTECTED REGION ID(DG645::BurpStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	DG645::BurpStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(DG645::DG645StateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	DG645::DG645StateAllowed.AdditionalMethods

}	//	End of namespace
