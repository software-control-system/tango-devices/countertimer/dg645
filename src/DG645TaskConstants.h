#include <yat4tango/DeviceTask.h>
#include <map>
#include <string>
#include <cmath>

namespace DG645_ns
{

const double NaN = ::sqrt(-1.0);
const std::string NaS = "NaS";

const std::size_t    MESSAGE_PERIOD  =  200;
const std::size_t    COMMAND_MSG     =  yat::FIRST_USER_MSG + 0;

/*!
*   \brief Struct containing the Task configuration, configuration is set by the main Device
*
*   \struct CurrentsConfig
*/
struct CurrentsConfig
{
    bool                is_socket_alive;
    std::string         status;
    Tango::DevState     state;
    
    Tango::DevShort     freq_div;
    Tango::DevUShort    freq_div_ab;
    Tango::DevUShort    freq_div_cd;
    Tango::DevUShort    freq_div_ef;
    Tango::DevUShort    freq_div_gh;
    
    Tango::DevDouble    delay_a;
    Tango::DevDouble    delay_b;
    Tango::DevDouble    delay_c;
    Tango::DevDouble    delay_d;
    Tango::DevDouble    delay_e;
    Tango::DevDouble    delay_f;
    Tango::DevDouble    delay_g;
    Tango::DevDouble    delay_h;
    
    Tango::DevDouble    width_ab;
    Tango::DevDouble    width_cd;
    Tango::DevDouble    width_ef;
    Tango::DevDouble    width_gh;
    
    Tango::DevUShort    burst_count;
};

struct StatusMap
{
public:
    StatusMap()
    {
        tmp_map ["0"]   =  "Normal operation";
        tmp_map ["10"]  =  "[Execution Errors] A parameter was out of range.";
        tmp_map ["11"]  =  "[Execution Errors] The action is illegal in the current mode. This might happen, for instance, if a single shot is requested when the trigger source is not single shot.";
        tmp_map ["12"]  =  "[Execution Errors] The requested delay is out of range.";
        tmp_map ["13"]  =  "[Execution Errors] The requested delay linkage is illegal.";
        tmp_map ["14"]  =  "[Execution Errors] The recall of instrument settings from nonvolatile storage failed. The instrument settings were invalid.";
        tmp_map ["15"]  =  "[Execution Errors] The requested action is not allowed because the instrument is locked by another interface.";
        tmp_map ["16"]  =  "[Execution Errors] The DG645 self test failed.";
        tmp_map ["17"]  =  "[Execution Errors] The DG645 auto calibration failed.";
        tmp_map ["30"]  =  "[Query Errors] Data in the output buffer was lost. This occurs if the output buffer overflows, or if a communications error occurs and data in output buffer is discarded.";
        tmp_map ["32"]  =  "[Query Errors] This is a communications error that occurs if the DG645 is addressed to talk on the GPIB bus, but there are no listeners. The DG645 discards any pending output.";
        tmp_map ["40"]  =  "[Equipement Dependent Errors] The ROM checksum failed. The firmware code is likely corrupted.";
        tmp_map ["41"]  =  "[Equipement Dependent Errors] Self test of offset functionality for output T0 failed.";
        tmp_map ["42"]  =  "[Equipement Dependent Errors] Self test of offset functionality for output AB failed.";
        tmp_map ["43"]  =  "[Equipement Dependent Errors] Self test of offset functionality for output CD failed.";
        tmp_map ["44"]  =  "[Equipement Dependent Errors] Self test of offset functionality for output EF failed.";
        tmp_map ["45"]  =  "[Equipement Dependent Errors] Self test of offset functionality for output GH failed.";
        tmp_map ["46"]  =  "[Equipement Dependent Errors] Self test of amplitude functionality for output T0 failed.";
        tmp_map ["47"]  =  "[Equipement Dependent Errors] Self test of amplitude functionality for output AB failed.";
        tmp_map ["48"]  =  "[Equipement Dependent Errors] Self test of amplitude functionality for output CD failed.";
        tmp_map ["49"]  =  "[Equipement Dependent Errors] Self test of amplitude functionality for output EF failed.";
        tmp_map ["50"]  =  "[Equipement Dependent Errors] Self test of amplitude functionality for output GH failed.";
        tmp_map ["51"]  =  "[Equipement Dependent Errors] Self test of FPGA communications failed.";
        tmp_map ["52"]  =  "[Equipement Dependent Errors] Self test of GPIB communications failed.";
        tmp_map ["53"]  =  "[Equipement Dependent Errors] Self test of DDS communications failed.";
        tmp_map ["54"]  =  "[Equipement Dependent Errors] Self test of serial EEPROM communications failed.";
        tmp_map ["55"]  =  "[Equipement Dependent Errors] Self test of the temperature sensor communications failed.";
        tmp_map ["56"]  =  "[Equipement Dependent Errors] Self test of PLL communications failed.";
        tmp_map ["57"]  =  "[Equipement Dependent Errors] Self test of DAC 0 communications failed.";
        tmp_map ["58"]  =  "[Equipement Dependent Errors] Self test of DAC 1 communications failed.";
        tmp_map ["59"]  =  "[Equipement Dependent Errors] Self test of DAC 2 communications failed.";
        tmp_map ["60"]  =  "[Equipement Dependent Errors] Self test of sample and hold operations failed.";
        tmp_map ["61"]  =  "[Equipement Dependent Errors] Self test of Vjitter operation failed.";
        tmp_map ["62"]  =  "[Equipement Dependent Errors] Self test of channel T0 analog delay failed.";
        tmp_map ["63"]  =  "[Equipement Dependent Errors] Self test of channel T1 analog delay failed.";
        tmp_map ["64"]  =  "[Equipement Dependent Errors] Self test of channel A analog delay failed.";
        tmp_map ["65"]  =  "[Equipement Dependent Errors] Self test of channel B analog delay failed";
        tmp_map ["66"]  =  "[Equipement Dependent Errors] Self test of channel C analog delay failed.";
        tmp_map ["67"]  =  "[Equipement Dependent Errors] Self test of channel D analog delay failed.";
        tmp_map ["68"]  =  "[Equipement Dependent Errors] Self test of channel E analog delay failed.";
        tmp_map ["69"]  =  "[Equipement Dependent Errors] Self test of channel F analog delay failed.";
        tmp_map ["70"]  =  "[Equipement Dependent Errors] Self test of channel G analog delay failed.";
        tmp_map ["71"]  =  "[Equipement Dependent Errors] Self test of channel H analog delay failed.";
        tmp_map ["80"]  =  "[Equipement Dependent Errors] Auto calibration of sample and hold DAC failed.";
        tmp_map ["81"]  =  "[Equipement Dependent Errors] Auto calibration of channel T0 failed.";
        tmp_map ["82"]  =  "[Equipement Dependent Errors] Auto calibration of channel T1 failed.";
        tmp_map ["83"]  =  "[Equipement Dependent Errors] Auto calibration of channel A failed.";
        tmp_map ["84"]  =  "[Equipement Dependent Errors] Auto calibration of channel B failed.";
        tmp_map ["85"]  =  "[Equipement Dependent Errors] Auto calibration of channel C failed.";
        tmp_map ["86"]  =  "[Equipement Dependent Errors] Auto calibration of channel D failed.";
        tmp_map ["87"]  =  "[Equipement Dependent Errors] Auto calibration of channel E failed.";
        tmp_map ["88"]  =  "[Equipement Dependent Errors] Auto calibration of channel F failed.";
        tmp_map ["89"]  =  "[Equipement Dependent Errors] Auto calibration of channel G failed.";
        tmp_map ["90"]  =  "[Equipement Dependent Errors] Auto calibration of channel H failed.";
        tmp_map ["91"]  =  "[Equipement Dependent Errors] Auto calibration of Vjitter failed.";
        tmp_map ["110"] =  "[Parsing Errors] The command syntax used was illegal. A command is normally a sequence of four letters, or a ???*???followed by three letters.";
        tmp_map ["111"] =  "[Parsing Errors] The specified command does not exist.";
        tmp_map ["112"] =  "[Parsing Errors] The specified command does not permit queries";
        tmp_map ["113"] =  "[Parsing Errors] The specified command can only be queried.";
        tmp_map ["114"] =  "[Parsing Errors] The parser detected an empty parameter.";
        tmp_map ["115"] =  "[Parsing Errors] The parser detected more parameters than allowed by the command.";
        tmp_map ["116"] =  "[Parsing Errors] The parser detected missing parameters required by the command.";
        tmp_map ["117"] =  "[Parsing Errors] The buffer for storing parameter values overflowed. This probably indicates a syntax error.";
        tmp_map ["118"] =  "[Parsing Errors] The parser expected a floating point number, but was unable to parse it.";
        tmp_map ["120"] =  "[Parsing Errors] The parser expected an integer, but was unable to parse it.";
        tmp_map ["121"] =  "[Parsing Errors] A parsed integer was too large to store correctly.";
        tmp_map ["122"] =  "[Parsing Errors] The parser expected hexadecimal characters but was unable to parse them.";
        tmp_map ["126"] =  "[Parsing Errors] The parser detected a syntax error in the command.";
        tmp_map ["170"] =  "[Communication Errors] A communication error was detected. This is reported if the hardware detects a framing, or parity error in the data stream.";
        tmp_map ["171"] =  "[Communication Errors] The input buffer of the remote interface overflowed. All data in both the input and output buffers will be flushed.";
        tmp_map ["254"] =  "[Other Errors] The error buffer is full. Subsequent errors have been dropped.";
    }
    
    const std::string& get_status(const std::string& argin, const std::string& default_string) const
    {
        //- Finding the string associated with the key
        std::map <std::string, std::string>::const_iterator it_map;
        it_map = tmp_map.find(argin);
        
        //- Checking if the key is in the database
        if( it_map != tmp_map.end() )
        {
            return it_map->second;
        }
        else
        {
            return default_string;
        }
    }
    
private:
    std::map<std::string, std::string> tmp_map;
};

} //namespace DG645_ns
